<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, java.net.*, java.io.*, java.util.*" %>

<body>
<center>
<h1>inventory_list</h1> <br>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// Statement 생성 및 연결 설정
	Statement stmt = conn.createStatement();
	
	
	int numb = 0;
	
	// ResultSet 선언 및 설정
	ResultSet rset1=stmt.executeQuery("select * from number;");
	
	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset1.next()){
		numb=rset1.getInt(1);	// 전체 레코드 저장
	}
	out.println(numb);
	// 자원 반환
	rset1.close();
	stmt.close();
	conn.close();
%>
</body>
