<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*, java.util.*" %>

<%		
	// 선택된 페이지 번호 참조
	String pageNum=request.getParameter("pageNum");
	if(pageNum==null){
		pageNum="1";
	}	
	int currentPage=Integer.parseInt(pageNum);	
	
	// 한페이지에 보일 레코드 숫자
	String pageSizeS=request.getParameter("pageSize");
	if(pageSizeS==null){
		pageSizeS="5";
	}	
	int pageSize=Integer.parseInt(pageSizeS);	
%>

<html>
<head>
<!-- css 스타일 선언 : a 링크 스타일 -->
<style>
	a:link { color: #000000; text-decoration: underline; }
	a:visited { color: #000000; text-decoration: none; }
	a:hover { color: #000000; text-decoration: underline; font-weight: bold;}
	a:active { color: #000000; text-decoration: none; font-weight: bold;}
	
	.p {
		padding-left: 10px;
	}
</style>

</head>
<body>
<center>
<h1>inventory_list</h1> <br>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// Statement 생성 및 연결 설정
	Statement stmt = conn.createStatement();
	
	// 변수 선언 및 초기값 설정
	int totalRecords=0;
	
	// ResultSet 선언 및 설정
	ResultSet rset1=stmt.executeQuery("select count(*) from inventory_table;");
	
	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset1.next()){
		totalRecords=rset1.getInt(1);	// 전체 레코드 저장
	}
	// 자원 반환
	rset1.close();
	stmt.close();
	conn.close();
%>
<%	
	// 총 페이지 수 계산
	int totalPage = 0;							// 총 페이지 변수 설정
	int R = totalRecords % pageSize;			
	if (R == 0){	
		totalPage=totalRecords/pageSize;		// 총 페이지 수 결정
	} else {
		totalPage=totalRecords/pageSize +1;		// 총 페이지 수 결정(나머지가 다를 경우)
	}

	int groupSize = 10;			// 한 그룹의 페이지 개수
	int currentGroup = 0;		// 현재 그룹 설정 초기화


	R = totalPage%groupSize;									// 현재 속한 그룹 설정
	if (R==0){
		currentGroup=currentPage/groupSize;
	} else {
		currentGroup=currentPage/groupSize+1;
	}
	
	currentGroup = (int)Math.floor((currentPage-1)/ groupSize);	// 현재 그룹 
	int groupStartPage = currentGroup*groupSize +1;				// 현 그룹 시작 페이지
	int groupEndPage = groupStartPage+groupSize-1;				// 현 그룹 끝 페이지
	
	if(groupEndPage>totalPage){									// 마지막 페이지 설정
		groupEndPage = totalPage;
	}
	
	int lineCount=0;	
	int fromPT =(currentPage-1)*pageSize+1; //시작 번호
%>
<%
	// 데이터베이스 연결
	conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// stmt 생성 및 연결 설정
	stmt = conn.createStatement();	
	
	// ResultSet 선언 및 설정
	ResultSet rset = stmt.executeQuery("select id,name,quantity,enrolldate,changedate from inventory_table order by id desc;");
%>
<!-- 목록 출력용 테이블 생성 -->
<table width=750 cellspacing=2 border=2>
	<tr align=center height=60>
		<td colspan=5><font size=4><b>(주)트와이스 재고 현황 - 전체현황</b></font></td>
	</tr>
	<tr height=40 align=center>
		<td width=100 >상품번호</td>
		<td width=250 >상품명</td>
		<td width=100 >현재 재고수</td>
		<td width=150 >재고파악일</td>
		<td width=150 >상품등록일</td>
	</tr>
<%
	// ResultSet 이 존재하는 동안 이하 반복문 실행	
	while(rset.next()){
		
		// 변수 증가
		lineCount++;
		
		// 변수값과 비교하여 break 시기 결정
		if(lineCount < fromPT) continue;
		if(lineCount >= fromPT+pageSize) break;
%>
		<!-- 전체 조회 개인별 조회를 위한 링크 설정 -->
		<!-- 파라미터 id; name; 설정 -->
		<tr height=40>
			<td align=center><%=Integer.toString(rset.getInt(1))%></p></td>
			<td class="p"><a href="view.jsp?id=<%=rset.getInt(1)%>"><%=rset.getString(2)%></a></p></td>			
			<td align=center><%=rset.getInt(3)%></p></td>
			<td align=center><%=rset.getString(4)%></p></td>
			<td align=center><%=rset.getString(5)%></p></td>
		</tr>
<%
		} 		
		// 자원 반환
		rset.close();
		stmt.close();
		conn.close();
%>

</table>
<br>
<!-- 페이징 위한 행 삽입 -->
<table>
<tr><td colspan="8"></td></tr>
	<tr>
		<td colspan="8"  height=40 align=center>
		<a href="list.jsp?pageNum=<%=1%>" style="text-decoration:none">First</a>&nbsp&nbsp;
<%			
	if(currentGroup >= 1){
%>
		<a href="list.jsp?pageNum=<%=groupStartPage-1%>" style="text-decoration:none">Previous</a>&nbsp&nbsp;
<%		
	}

	int groupPageCount = groupSize;		// 그룹 당 페이지 수
	int index = groupStartPage;			// 현 그룹 시작 페이지	

	// 그룹당 페이지수가 0보다 크고 && 페이지 수가 그룹의 마지막 페이지 번호보다 작거나 같을때 반복문 실행
	while(groupPageCount>0 && index<=groupEndPage){				
%>
		<!-- <a href="wifi.jsp?pageNum=< %=index%>" style="text-decoration:none">< %=index%></a>&nbsp&nbsp; -->
<%	
		if(index==currentPage){
%>
			<b>[&nbsp<a href="list.jsp?pageNum=<%=index%>" style="text-decoration:none"><%=index%></a>&nbsp]&nbsp&nbsp;</b>
<%			
		} else {
%>
			<a href="list.jsp?pageNum=<%=index%>" style="text-decoration:none"><%=index%></a>&nbsp&nbsp;
<%			
		}
		index = index+1;					// 페이지 번호 1씩 증가
		groupPageCount=groupPageCount-1;	// 읽어야 할 그룹 페이지 하나씩 감소
	}

	if(index<=totalPage){
%>
		<a href="list.jsp?pageNum=<%=index%>" style="text-decoration:none">Next</a>&nbsp&nbsp;
<%	
	}
	
	if(currentGroup >= 0){
%>
		<a href="list.jsp?pageNum=<%=totalPage%>" style="text-decoration:none">Last</a>&nbsp;
<%		
	}
%>
		</td>
	</tr>
</table>
<br>
<!-- 버튼용 테이블 생성 -->
<table width=750 cellspacing=1 border=0>
	<tr height=30>
		<td >
		</td>
		<td width=150><p align=center>			
			<input type=button value="신규등록" OnClick=window.location="insert.jsp">			
		</p></td>
	</tr>
</table>	
</center>
</body>
</html>