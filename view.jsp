<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*" %>

<html>
<head>

<script language="javascript">
	function submitForm(mode){
		if(mode=="update"){
			fm.action="update.jsp";
		} else if(mode=="delete"){
			fm.action="delete.jsp";
		}
		fm.submit();
	}
</script>

<!-- css 스타일 클래스 선언 : 버튼 스타일 -->
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
</style>

<style>
	.p {
		padding-left: 10px;
	}
	.input {
		height: 25px; 
	}
	.inputTitle {
		height: 20px; width: 400px;
	}
	.textArea {
		resize: none; height: 30px; width: 480px; 
	}
</style>
</head>
<body>
<center>
<h1>inventory_view</h1> <br>
<!-- 데이터베이스 연결; 변수 선언; 결과값 저장; -->
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// list.jsp 의 id 값을 받기 위한 파라미터값 요청
	// ResultSet 의 결과값을 받기 위한 변수 선언 및 초기값 설정
	String idS=request.getParameter("id");
	int id = Integer.parseInt(idS);			// 문자열 형태를 정수형 변수로 변환하여 변수값을 저장	
	String name="";
	int quantity = 0;
	String enrollDate="";
	String changeDate="";
	String content="";
	String img="";
	String noDate=null;

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	// Statement 선언 및 설정
	Statement stmt1 = conn.createStatement();

	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select * from inventory_table where id="+id+";");

	// ResultSet 이 존재하는 동안 이하 반복문 실행
	while(rset1.next()){		
		name=rset1.getString(2);
		quantity=rset1.getInt(3);
		enrollDate=rset1.getString(4);
		changeDate=rset1.getString(5);
		content=rset1.getString(6);
		img=rset1.getString(7);
	}
	// 자원 반환
	rset1.close();
	stmt1.close();
	conn.close();
%>
<form method=post name="fm">
<!-- 결과값 출력 -->
<table width=750 cellspacing=1 border=2 >
	<tr height=60>
		<td colspan=2><p align=center><font size=4><b>(주)트와이스 재고 현황 - 상품상세</b></font></p></td>
	</tr>
	<tr height=40>
		<td class="p" width=200>상품번호</td>
		<td class="p" ><%=id%><input type=hidden name="id" value=<%=id%>></td>
	</tr>
	<tr height=40>
		<td class="p">상품명</td>
		<td class="p" ><%=name%><input type=hidden name="name" value=<%=name%>></td>
	</tr>
	<tr height=40>
		<td class="p" >재고 현황</td>
		<td class="p" ><%=quantity%><input type=hidden name="quantity" value=<%=quantity%>></td>
	</tr>
	<tr height=40>
		<td class="p" >상품등록일</td>
		<td class="p" ><%=enrollDate%><input type=hidden name="enrollDate" value=<%=enrollDate%>></td>
	</tr>
	<tr height=40>
		<td class="p" >재고등록일</td>
		<td class="p" ><%=changeDate%><input type=hidden name="changeDate" value=<%=changeDate%>></td>
	</tr>
	<tr height=40>
		<td class="p" >상품설명</td>
		<td class="p" >
			<textarea class="textArea" readonly ><%=content%></textarea>
			<input type=hidden name="content" value=<%=content%>>
		</td>
	</tr>
	<tr height=100>
		<td class="p" >상품사진</td>
		<td class="p" >
			<img src="/upload/<%=img%>" width=360><%=img%>
			<input type=hidden name="path" value=<%=img%>></input>
		</td>
	</tr>
</table>
<br>

<!-- 파일 이동 위한 버튼 출력 및 설정 : 재고현황; 상품삭제; 재고수정 -->
<table width=750 cellspacing=1 border=0 >
	<tr height=30>
		<td width=450>
		</td>
		<td width=100 align=center>
			<input type=button value="재고 현황" OnClick=location.href="list.jsp">
		</td>
		<td width=100 align=center>
			<input type=button value="상품삭제" OnClick="submitForm('delete')">
		</td>
		<td width=100 align=center>
			<input type=button value="재고수정" OnClick="submitForm('update')">
		</td>
	</tr>
</table>
</form>
</body>
</html>