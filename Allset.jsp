<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<%@ page import="java.sql.*, java.io.*, java.text.*, java.util.*" %>

<html>
<head>
</head>
<body>
<h1>Allset inventory_table</h1>
<%
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	// java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	// java.util.Date 클래스( Date constructors )
	// -> Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String today = df.format(new java.util.Date());	

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	//Statement 생성 후 연결
	Statement stmt = conn.createStatement();
	Statement stmt1 = conn.createStatement();
	
	ResultSet rset1 = stmt1.executeQuery("select id from inventory_table;");
	
	int startID=1000001;
	int addID=0;
	addID=startID;
	while(rset1.next()){
		if(rset1.getInt(1)==addID){
			addID=addID+1;
		} else {
			break;
		}		
	}
	
%>
<%
	String sql="";
	
	// 생성된 테이블에 데이터 입력
	for (int i=1; i<30; i++){
		int q = 10+i;		
		sql="insert into inventory_table (id,name,quantity,enrolldate,changedate,content) values("+addID+",'상품"+i+"',"+q+",'"+today+"','"+today+"','상품설명"+i+"')";
		stmt.execute(sql);
		addID=addID+1;
	}
	out.println("데이터 삽입 확인");
	
	// 자원 반환
	stmt.close();
	conn.close();
%>
</body>
</html>