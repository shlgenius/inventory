<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, java.io.*, java.text.*, java.util.*" %>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>

<html>
<head>

<script language="javascript">
	function submitForm(mode){
		var quantity = document.forms[0].quantity.value;
		var pattern_quantity=/[0-9]+$/gi;
		if (quantity > 0 && quantity < 2147483647){
			if (pattern_quantity.test(quantity)==false){
				alert("잘못된 입력");
				document.forms[0].quantity.value=quantity;
				document.forms[0].quantity.focus();
				return false;
			}
		} else {
			alert("범위를 벗어남");
			document.forms[0].quantity.value=quantity;
			document.forms[0].quantity.focus();
			return false;
		}
		
		if(mode=="update"){
			fm.action="write.jsp?key=UPDATE";
		} else if(mode=="delete"){
			fm.action="delete.jsp";
		}
		fm.submit();
	}
</script>

<!-- css 스타일 클래스 선언 : 버튼 스타일 -->
<style>
	.button {
		width: 80px;
		height: 40px;
		padding: 2px 2px 2px 2px; /* 상, 우, 좌, 하 */
		text-align: center;
		border: 1px solid #000000;		
		background-color : #D8D8D8;	
		
		font-size: 15px;
		font-weight: bold;
	}
</style>

<style>
	.input {
		height: 25px; 
	}
	.inputTitle {
		height: 20px; width: 400px;
	}
	.textArea {
		resize: none; height: 30px; width: 480px; 
	}
</style>
</head>
<body>
<center>
<h1>inventory_table UPDATE</h1> <br>
<!-- 데이터베이스 연결; 변수 선언; 결과값 저장; -->
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// view.jsp 의 파라미터값 요청	
	String idS=request.getParameter("id");
	int id = Integer.parseInt(idS);			// 문자열 형태를 정수형 변수로 변환하여 변수값을 저장	
	String name=request.getParameter("name");
	String quantityS = request.getParameter("quantity");
	int quantity = Integer.parseInt(quantityS);			// 문자열 형태를 정수형 변수로 변환하여 변수값을 저장	
	String enrollDate=request.getParameter("enrollDate");
	String changeDate=request.getParameter("changeDate");
	String content=request.getParameter("content");
	String img=request.getParameter("path");
	
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	// java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	// java.util.Date 클래스( Date constructors )
	// -> Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String todayUpdate = df.format(new java.util.Date());	
%>

<!-- cos.jar webapps에 붙여 넣은 후 enctype="multipart/form-data" 입력 -->
<!-- import="com.oreilly.servlet.MultipartRequest" -->
<!-- import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" -->
<!-- enctype 입력 해야 MultipartRequest 사용 가능-->
<form method=post name="fm" enctype="multipart/form-data">

<!-- 결과값 출력 -->
<table width=750 cellspacing=1 border=2 >
	<tr height=60>
		<td colspan=2><p align=center><font size=4><b>(주)트와이스 재고 현황 - 재고수정</b></font></p></td>
	</tr>
		<tr height=40>
		<td width=200 >상품번호</td>
		<td><%=id%><input type=hidden name="id" value=<%=id%> readonly ></input></td>
	</tr>
	<tr height=40>
		<td>상품명</td>
		<td><%=name%><input type=hidden name="name" value=<%=name%>></td>		
	</tr>
	<tr height=40>
		<td >재고 현황</td>
		<td><input class="input" type=number name="quantity" value=<%=quantity%>></td>
	</tr>
	<tr height=40>
		<td >상품등록일</td>
		<td><%=enrollDate%><input type=hidden name="enrollDate" value=<%=enrollDate%>></input></td>
	<tr height=40>
		<td >재고등록일</td>
		<td><%=todayUpdate%><input type=hidden name="changeDate" value=<%=todayUpdate%>></input></td>
	</tr>
	<tr height=40>
		<td >상품설명</td>
		<td><%=content%><input type=hidden name="content" value=<%=content%>></input></td>
	</tr>
	<tr height=100>
		<td>상품사진</td>
		<td>
			<img src="/upload/<%=img%>" width=300 height=300>
			<input type=hidden name="path" value=<%=img%>>	
		</td>
	</tr>
</table>
<br>

<!-- 파일 이동 위한 버튼 출력 및 설정 : 목록; 수정 -->
<table width=750 cellspacing=1 border=0 >
	<tr height=30>
		<td width=450>
		</td>
		<td width=100 align=center>
			<input type=button value="재고현황" OnClick=location.href="list.jsp">
		</td>
		<td width=100 align=center>
			<input type=button value="상품삭제" OnClick="submitForm('delete')">
		</td>
		<td width=100 align=center>
			<input type=button value="재고수정" OnClick="submitForm('update')">
		</td>
	</tr>
</table>
</form>
</center>
</body>
</html>