<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" language="java" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.net.*, java.io.*" %>
<html>
<head>
</head>
<body>
<h1>create table inventory_table</h1>
<%
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	//stmt 생성 후 연결
	Statement stmt = conn.createStatement();
	
	// 예외 처리 
	try{
		
		// 쿼리문 실행
		stmt.execute("create table inventory_table (id int not null primary key, name varchar(70) not null, quantity int not null, enrolldate date not null, changedate date not null, content varchar(200), img varchar(100));");
		
		// 자원반환
		stmt.close();
		conn.close();
		
		out.println("OK");
	} catch(SQLException e){
		
		// 예외 발생시 출력문
		out.println("실패 - 테이블이 이미 존재");
	}
%>
</body>
</html>