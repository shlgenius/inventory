<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, java.io.*, java.text.*, java.util.*" %>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>

<html>
<head>
<!-- css 스타일 클래스 선언 : input 부분; textArea 부분 -->
<style>
	.input {
		height: 25px; 
	}
	.inputTitle {
		height: 25px; width: 200px;
	}
	.textArea {
		resize: none; height: 30px; width: 480px; 
	}
	.img_wrap {
		width: 360px;  
	}
	.img_wrap img {
		max-width: 100%;
	}
</style>

<!-- 함수 선언 : 선언한 form 의 action 방식 설정 -->
<script language="JavaScript">
	function submitForm(model){
		
		var name = document.forms[0].name.value;
		var quantity = document.forms[0].quantity.value;
		var content = document.forms[0].content.value;
		var fileName = document.forms[0].fileName.value;
		
		var han=/^[가-힣]*$/;
		var eng=/^[a-zA-Z]*$/;
		var pattern_name=/^[가-힣a-zA-Z0-9]{1,10}$/gi;
		var pattern_special=/[~!^{}<>;\/?]/gi;
		var pattern_quantity=/[0-9]+$/gi;
		var pattern_content=/[`~!@#$%^&*|\\\'\";:\/?]/gi;
		var pattern_blank = /^\s+|\s+$/g;
		var pattern_special_filename=/[~!^{}<>;\/?]/gi;
		
		if( pattern_blank.test(name) == true ){
			alert("앞 뒤 공백 입력 불가");
			document.forms[0].name.focus();
			return false;
		}		
		if(name==''|| name==null){
			alert("상품 이름 없음");	
			document.forms[0].name.focus();			
			return false;
		}		
		if( pattern_blank.test(name) == true || pattern_blank.test(content) == true || pattern_blank.test(fileName) == true){
			alert("앞 뒤 공백 입력 불가");			
			return false;
		}				
		if (pattern_name.test(name)==false && pattern_special.test(name)==true){
			alert("잘못된 입력");
			document.forms[0].name.value=name;
			document.forms[0].name.focus();
			return false;
		}
		if (pattern_content.test(content)==true){
			alert("잘못된 입력");
			document.forms[0].content.value=content;
			document.forms[0].content.focus();
			return false;
		}
		if(quantity==''|| quantity==null){
			alert("재고 필수 입력");	
			document.forms[0].quantity.focus();			
			return false;
		}	
		if (quantity > 0 && quantity < 2147483647){
			if (pattern_quantity.test(quantity)==false){
				alert("잘못된 입력");
				document.forms[0].quantity.value=quantity;
				document.forms[0].quantity.focus();
				return false;
			}
		} else {
			alert("범위를 벗어남");
			document.forms[0].quantity.value=quantity;
			document.forms[0].quantity.focus();
			return false;
		}
		if (fileName==''|| fileName==null){
			alert("상품 사진 없음");	
			document.forms[0].fileName.focus();			
			return false;
		}
		if (pattern_special_filename.test(fileName)==true){
			alert("파일에서 특수문자 제거");	
			document.forms[0].fileName.focus();			
			return false;
		}
		if (pattern_blank.test(fileName) == true ){
			alert("앞 뒤 공백 입력 불가");
			document.forms[0].fileName.focus();
			return false;
		}	
		
		fm.action = "write.jsp?key=INSERT";
		fm.submit();
	}
</script>

</head>
<body>
<center>
<h1>inventory_insert</h1> <br>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	// java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	// java.util.Date 클래스( Date constructors )
	// -> Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String today = df.format(new java.util.Date());	
%>

<%	
	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl", "root", "111");
	
	// Statement 생성 및 연결 설정
	Statement stmt1 = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	
	// ResultSet 선언 및 설정
	ResultSet rset1 = stmt1.executeQuery("select id from inventory_table;");
//	stmt2.execute("set time_zone='Asia/Seoul';");
//	ResultSet rset2 = stmt2.executeQuery("select date_format(now(),'%Y-%m-%d');");
	
	String now="";
	
//	while(rset2.next()){
//		now=rset2.getString(1);
//	}
	
	// 아이디 초기값 설정
	int startID=1000001;
	int addID=0;
	addID=startID;
	while(rset1.next()){
		if(rset1.getInt(1)==addID){
			addID=addID+1;
		} else {
			break;
		}		
	}
%>

<!-- cos.jar webapps에 붙여 넣은 후 enctype="multipart/form-data" 입력 -->
<!-- import="com.oreilly.servlet.MultipartRequest" -->
<!-- import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" -->
<!-- enctype 입력 해야 MultipartRequest 사용 가능-->

<!-- form 선언 : post 방식; name='fm' -->
<form method=post name="fm" enctype="multipart/form-data">

<!-- 테이블 출력 -->
<!-- 쓰기 실행시 id; title; quantity; writeDate; content; 를 파라미터 값으로 전달 -->
<table width=750 cellspacing=1 border=2 >
	<tr height=60>
		<td colspan=2><p align=center><font size=4><b>(주)트와이스 재고 현황 - 상품등록</b></font></p></td>
	</tr>
	<tr height=40>
		<td width=200 >상품번호</td>
		<td><%=addID%><input type=hidden name="id" value=<%=addID%> readonly ></input></td>
	</tr>
	<tr height=40>
		<td>상품명</td>
		<td><input type=text name="name" value="" class="inputTitle"></input></td>		
	</tr>
	<tr height=40>
		<td>재고 현황</td>
		<td><input type=number name="quantity" value="" class="input"></input></td>
	</tr>
	<tr height=40>
		<td>상품등록일</td>
		<td><%=today%><input type=hidden name="enrollDate" value=<%=today%>></input></td>
	</tr>
	<tr height=40>
		<td>재고등록일</td>
		<td><%=today%><input type=hidden name="changeDate" value=<%=today%>></input></td>
	</tr>
	<tr height=40>
		<td>상품설명</td>
		<td><input name="content" cols=70 rows=100 value="" class="textArea"></input></td>		
	</tr>
	<tr height=80>
		<td>상품사진</td>
		<td height=80>
			<article>
				<p id="status"></p>
				<p><input type=file name="fileName"></p>
				<div id="holder" class="img_wrap"></div>
			</article>
		</td>
	</tr>
<script>
	//일시적으로 이미지를 띄워준다
	var upload = document.getElementsByTagName('input')[6],
    holder = document.getElementById('holder'),
    state = document.getElementById('status');

	upload.onchange = function (e) {
	e.preventDefault();

	var file = upload.files[0],
    reader = new FileReader();
	reader.onload = function (event) {
    var img = new Image();
    img.src = event.target.result;
    
    if (img.width > 560) { // holder width
      img.width = 560;
    }
    holder.innerHTML = '';
    holder.appendChild(img);
	};
	reader.readAsDataURL(file);

	return false;
	};
</script>
	
</table>
<br>


<!-- 버튼용 테이블 생성 -->
<table width=750 cellspacing=1 border=0 >
	<tr height=40>
		<td width=600>
		</td>
		<td width=75 align=left>
			<!-- 취소 버튼 생성 : OnClick 방식 활용 -->
			<!-- 쓰기 버튼 생성 : OnClick 방식 활용 : submitForm() 함수 호출 -->
			<input type=button value="취소" OnClick=location.href="list.jsp"></input>
		</td>
		<td width=75 align=left>
			<input type=button value="완료" OnClick="submitForm('write')"></input>
		</td>		
	</tr>
</table>
</form>
</center>
</body>
</html>