<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType="text/html; charset=utf-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*,javax.sql.*,java.io.*" %>

<html>
<head>
</head>
<body>
<h1>inventory_table DELETE</h1> <br>
<center>
<%
	// 한글 설정
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");

	// view.jsp 의 파라미터값 요청	: idl name, img
	String idS=request.getParameter("id");
//	int id = Integer.parseInt(idS);
	String name=request.getParameter("name");	
	String img=request.getParameter("path");

	// 데이터베이스 연결
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");

	// Statement 생성 및 연결 설정
	Statement stmt1 = conn.createStatement();
	
	// sql 실행 (inventory_table 의 해당 id의 데이터 삭제
	stmt1.execute("delete from inventory_table where id="+idS+";");
	
	// 파일 삭제를 위한 변수 선언 및 초기값 설정	
	String fileName=img;										// 파일 이름
	String dirPath="/var/lib/tomcat8/webapps/ROOT/upload/";		// 파일 저장 경로
	String filePath=dirPath+fileName;							// 전체 경로
	
	File f = new File(filePath);	// 파일 객체 생성
	if(f.exists()) {				// 파일이 존재할 경우 파일 삭제 실행
		f.delete();
	}
	
	out.println(filePath);
	
//	stmt1.execute("set @cnt=1000000;");
//	stmt1.execute("update gongji set gongji.id=@cnt:=@cnt+1;");

	// 자원 반환
	stmt1.close();
	conn.close();
%>

<!-- post 방식 form 설정 -->
<form method=post name="fm">
<table width=750 cellspacing=1 border=2 >
	<tr height=60>
		<td colspan=2><p align=center><font size=4><b>(주)트와이스 재고 현황 - 상품삭제</b></font></p></td>
	</tr>
	<tr height=60>
		<td><p align=center><font size=4><b>[<%=name%>] 상품이 삭제되었습니다.</b></font></p></td>
	</tr>
	<tr height=40>
		<td>
			<input type=button value="재고 현황" OnClick=location.href="list.jsp">
		</td>
	</tr>
</table>
</form>
</center>
</body>
</html>