<!-- html과 jsp에서 한글처리 지시 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>

<!-- 필요한 부분 import -->
<%@ page import="java.sql.*, javax.sql.*, java.io.*, java.text.*, java.util.*" %>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>
<%@page import="java.text.SimpleDateFormat" %>

<html>
<head>
</head>
<body>
<center>
<h1>inventory_write</h1> <br>
<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
%>

<% 
    // request.getRealPath();를 출력한 결과
	// /var/lib/tomcat8/webapps/ROOT/ 
	// 여기에 파일을 업로드할 디렉토리를 지정
	String uploadPath=request.getRealPath("/upload");
	
	int size = 5*512*512;	// 그림파일 최대크기 지정	
	String filename1="";	
	
	// MultipartRequest는 
	// page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"
	// page import="com.oreilly.servlet.MultipartRequest"
	// 를 요구한다.
	// 또한 기존 입력 폼에서 enctype="multipart/form-data"를 사용하면 request를 multi로 받아야 한다.
	MultipartRequest multi=new MultipartRequest(request,uploadPath,size,"UTF-8",new DefaultFileRenamePolicy());
	// DefaultFileRenamePolicy는 파일 이름 중복일 경우 파일 이름 재설정
	
	String keyS = multi.getParameter("key");
	String idS = multi.getParameter("id");
//	int id = Integer.parseInt(idS);
	String name = multi.getParameter("name");
	String quantity = multi.getParameter("quantity");
	String enrollDate = multi.getParameter("enrollDate");
	String changeDate = multi.getParameter("changeDate");
	String content = multi.getParameter("content");	
	
	if(keyS.equals("INSERT")){
	Enumeration files = multi.getFileNames();	// Enumeration: page import="java.util.*" 를 요구한다.
	String file1 = (String)files.nextElement();
	filename1 = multi.getFilesystemName(file1);
    }
%>

<%
	// java.text.SimpleDateFormat 클래스 : 지정된 날짜를 문자열로 변환하는 클래스
	// java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	df.setTimeZone(TimeZone.getTimeZone("Asia/Seoul"));
	
	// java.util.Date 클래스( Date constructors )
	// -> Date() -> 생성자 Date 는 현재 날짜와 시간으로 객체를 초기화 (시간은 밀리 초 단위로 측정)
	String today = df.format(new java.util.Date());	

	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/shl","root","111");
	
	Statement stmt1 = conn.createStatement();
	
	if(keyS.equals("INSERT")){
		stmt1.execute("insert into inventory_table (id, name, quantity, enrolldate, changedate, content, img) values ("+idS+",'"+name+"',"+quantity+",'"+enrollDate+"','"+changeDate+"', '"+content+"', '"+filename1+"');");
	} else if (keyS.equals("UPDATE")){
//		stmt1.execute("set time_zone='Asia/Seoul';");
		stmt1.execute("update inventory_table set quantity="+quantity+", changedate='"+today+"' where id = "+idS+";");
	}
//	stmt1.execute("set @cnt = 1000001;");
//	stmt1.execute("update gongji set gongji.id=@cnt:=@cnt+1;");

	stmt1.close();
	conn.close();	
%>
<script language="javascript">
	location.href="list.jsp";
</script>
<%
	
%>
</center>
</body>
</html>